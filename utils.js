﻿var utils = {

    clamp: function (value, min, max) {
        return Math.min(Math.max(value, min), max);
    },

    randomIntFromInterval: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    },
    randomIntExceptZero: function (min, max) {
        var result = this.randomIntFromInterval(min, max);

        if (result > -0.5 && result < 0.5) {
            return this.randomIntExceptZero(min, max);
        }
        else {
            return result;
        }
    },


    randomFloatFromInterval: function (min, max) {
        return Math.random() * (max - min + 1) + min;
    },

    square: function (a) {
        return a * a;
    }

}