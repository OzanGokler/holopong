﻿var singlePlayer = function () { };

singlePlayer.prototype = {
    ball : {
        x: 180,
        y: 180,
        radius: 5,
        speed: 1,
        speedOffset: 0.1,
        maxSpeed: 6,
        angle: 0,
        minAngleOffset: 2,
        maxAngleOffset: 4,
        speedDirection: 1
    },
    gameScene : {
        x: 180,
        y: 180,
        radius: 150
    },
    collisionCircle : {
        x: 180,
        y: 180,
        radius: 120
    },
    player : {
        x: 0,
        y: 0,
        width: 50,
        height: 10,
        angle: 0,
        turnSpeed: 2,
        point : 0
    },
    collisionRadius: (125) * (125),

    update: function()
    {
        this.ball.x += Math.cos(this.ball.angle) * this.ball.speed * this.ball.speedDirection;
        this.ball.y += Math.sin(this.ball.angle) * this.ball.speed * this.ball.speedDirection;



        this.player.x = this.collisionCircle.x + this.collisionCircle.radius * Math.sin(this.player.angle * Math.PI / 180);
        this.player.y = document.getElementById("canvas").height - (this.collisionCircle.y + this.collisionCircle.radius * Math.cos(this.player.angle * Math.PI / 180));

        if (this.innerCircleCollision()) {
            if (this.coll()) {
                console.log("hit!");
                this.onPlayerSuccess();
                this.ball.speedDirection = -1 * this.ball.speedDirection;
                this.ball.angle -= utils.randomIntExceptZero(this.ball.minAngleOffset, this.ball.maxAngleOffset) / 10;

                if (this.ball.speed < this.ball.maxSpeed) {
                    this.ball.speed += this.ball.speedOffset;
                };
            };
        };     
    },

    onPlayerSuccess: function()
    {
        this.player.point += 1;
        console.log(this.player.point);
    },

    innerCircleCollision: function () 
    {
        var dx = this.ball.x - this.collisionCircle.x;
        var dy = this.ball.y - this.collisionCircle.y;

        return dx * dx + dy * dy > this.collisionRadius;
    },
    coll: function () 
    {
        var dx = this.ball.x - this.player.x;
        var dy = this.ball.y - this.player.y;

        return dx * dx + dy * dy <= this.player.width * this.player.width;
    },

    reset: function()
    {
        this.ball.x = 180;
        this.ball.y = 180;
        this.ball.radius = 5;
        this.ball.speed = 1;
        this.ball.speedOffset = 0.1;
        this.ball.maxSpeed = 6;
        this.ball.angle = 0;
        this.ball.minAngleOffset = 2;
        this.ball.maxAngleOffset = 4;
        this.ball.speedDirection = 1;

        this.player.x= 0;
        this.player.y = 0;
        this.player.width = 50;
        this.player.height = 10;
        this.player.angle = 0;
        this.player.turnSpeed = 2;
        this.player.point = 0;

        this.gameScene.x = 180;
        this.gameScene.y = 180;
        this.gameScene.radius = 150;

        this.collisionCircle.x = 180;
        this.collisionCircle.y = 180;
        this.collisionCircle.radius = 120;

        this. collisionRadius = (125) * (125);
    }
}

var _sp = new singlePlayer();
