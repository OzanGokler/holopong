﻿window.onload = function ()
{
    var canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d"),
        width = canvas.width = 360,
        height = canvas.height = 360;

    var menuStart = true;
    var singlePlayer = true;
    var multiPlayer = false;
    var singlePlayerStart = false;
    var multiPlayerStart = false;

    var turningLeft = false, turningRight = false;
    document.body.addEventListener("keydown", function (event) {
        switch (event.keyCode) {
            case 37: // left
                turningLeft = true;
                break;
            case 39: // right
                turningRight = true;
            default:
                break;
        }
    });

    document.body.addEventListener("keyup", function (event) {
        switch (event.keyCode) {
            case 37: // left
                turningLeft = false;
                break;
            case 39: // right
                turningRight = false;
                break;
                
            case 38: // up
                if (menuStart)
                {
                    singlePlayer = true;
                    multiPlayer = false;
                }               
                break;
            case 40: // down
                if (menuStart)
                {
                    singlePlayer = false;
                    multiPlayer = true;
                }              
                break;

            case 32: // space
                console.log("spacae");
                if (singlePlayer === true)
                {
                    singlePlayerStart = true;
                    multiPlayerStart = false;
                    menuStart = false;
                }
                if (multiPlayer === true)
                {
                    singlePlayerStart = false;
                    multiPlayerStart = true;
                    menuStart = false;
                }
                break;
            case 27: //esc
                menuStart = true;
                singlePlayerStart = false;
                multiPlayerStart = false;
                _sp.reset();
                break;
               
        }
    });

    update();

    function update()
    {
        //MENU
        if (menuStart)
        {
            menuDraw();
        }

        //SINGLE PLAYER
        if (singlePlayerStart)
        {
            _sp.update();
            singlePlayerGameDraw();
            if (turningRight) {
                _sp.player.angle = (_sp.player.angle + _sp.player.turnSpeed) % 360;
            }

            if (turningLeft) {
                _sp.player.angle = (_sp.player.angle - _sp.player.turnSpeed) % 360;
            }
        }
        //MULTIPLAYER
        if (multiPlayerStart)
        {
            multiplayerPlayerGameDraw();
        }
        
        requestAnimationFrame(update);

    
        
        
    }

    function gameBackground()
    {
        context.beginPath();
        context.fillStyle = "#f4f4f4";
        context.strokeStyle = "#000";
        context.strokeRect(0, 0, width, height);
        context.fillRect(0, 0, width, height);
        context.stroke();
        context.fill();
    }
    function menuDraw()
    {
        context.clearRect(0, 0, width, height);

        context.save();
        //draw background
        gameBackground();

            //Single player text           
            context.beginPath();
            if (singlePlayer) {
                context.font = "20px Arial";
                context.fillStyle = "blue";
                context.fillText("* SINGLE PLAYER *", width / 2, height / 2 - 20);
            }
            else {
                context.font = "16px Arial";
                context.fillStyle = "black";
                context.fillText("SINGLE PLAYER", width / 2, height / 2 - 20);
            }
            context.textAlign = "center";           
            context.stroke();
            context.fill();
            context.save();

            //MULTIPLAYER
            context.beginPath();
            if (multiPlayer) {
                context.font = "20px Arial";
                context.fillStyle = "blue";
                context.fillText("* MULTI PLAYER *", width / 2, height / 2 + 20);
            }
            else {
                context.font = "16px Arial";
                context.fillStyle = "black";
                context.fillText("MULTI PLAYER", width / 2, height / 2 + 20);
            }
            context.textAlign = "center";            
            context.stroke();
            context.fill();
            context.save();
    }

    function singlePlayerGameDraw() {
        context.clearRect(0, 0, width, height);

        context.save();

        //SINGLE PLAYER

        //draw background
        gameBackground();

        //draw Score
           context.font = "20px Arial";
            context.fillStyle = "green";
            context.textAlign = "center";
            context.fillText(_sp.player.point, width / 2, height / 2 +10);
            context.fill();

            //draw game circle
            context.beginPath();
            context.strokeStyle = "#000";
            context.arc(_sp.gameScene.x,
                        _sp.gameScene.y,
                        _sp.gameScene.radius,
                        0, Math.PI * 2, false);
            context.stroke();
            

            //draw ball
            context.beginPath();
            context.strokeStyle = "#c2c";
            context.fillStyle = "#3c3";
            context.arc(_sp.ball.x,
                        _sp.ball.y,
                        _sp.ball.radius,
                        0, Math.PI * 2, false);
            context.stroke();
            context.fill();

            //Player draw
            // first save the untranslated/unrotated context
            context.save();

            // move the rotation point to the center of the rect
            context.translate(width / 2, height / 2);

            // rotate the rect
            context.rotate(_sp.player.angle * Math.PI / 180);

            // draw the rect on the transformed context
            // Note: after transforming [0,0] is visually [x,y]
            //       so the rect needs to be offset accordingly when drawn
            context.rect(155 - width / 2, 40 - height / 2, _sp.player.width, _sp.player.height);


            context.fillStyle = "gold";
            context.fill();


        // restore the context to its untranslated/unrotated state
        context.restore();
    }

    function multiplayerPlayerGameDraw()
    {      
        context.clearRect(0, 0, width, height);
        context.save();
        gameBackground();
        context.beginPath();    
        context.font = "20px Arial";
        context.fillStyle = "Red";
        context.textAlign = "center";
        context.fillText("COMING SOON!!!", width / 2, height / 2 - 20);
        context.stroke();

        context.fill();
    }
};